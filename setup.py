from setuptools import setup,find_packages
setup(name='pyjrdatahost',
      version='0.1',
      description='python data server.',
      url='https://gitee.com/hzy15610046011/python_data_host',
      author='Zhanyi Hou',
      author_email='3120388018@qq.com',
      license='MuLan2.0',
      packages=find_packages(),
      include_package_data = True
      )
