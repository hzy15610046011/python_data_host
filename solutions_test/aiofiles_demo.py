import aiofiles
import threading
import asyncio
import time
# 异步写入文件流并不是推荐的方法。！！
N=10000
async def hello():
    async with aiofiles.open('/media/hzy/程序/novalide/forgitcommit/datahost/demos/class.csv', mode='r') as f:
        contents = await f.read()


def syncopen():
    with open('/media/hzy/程序/novalide/forgitcommit/datahost/demos/class.csv','r') as f:
        contents = f.read()

def run_2():
    t0=time.time()
    for i in range(N):
        syncopen()
    t1=time.time()

    print(t1-t0)

def run():
    t0=time.time()
    loop = asyncio.new_event_loop()
    tasks = [hello() for i in range(N)]

    loop.run_until_complete(asyncio.wait(tasks))

    loop.close()
    t1=time.time()

    print(t1-t0)
th=threading.Thread(target=run)
th.start()
th.join()
time.sleep(0.5)
run_2()