# 不能用于写入文件。
# 或许可以用于异步请求吧。
import threading
import asyncio
import time



async def hello():
    print('Hello world! (%s)' % threading.currentThread())
    f = open('/media/hzy/程序/novalide/forgitcommit/datahost/demos/class.csv','r')
    await f# asyncio.sleep(1)# 运行到这一步之后，程序开始重新运行。
    f.close()
    print('Hello again! (%s)' % threading.currentThread())

def run_2():
    t0=time.time()
    loop = asyncio.new_event_loop()
    tasks = [hello() for i in range(10)]

    loop.run_until_complete(asyncio.wait(tasks))

    loop.close()
    t1=time.time()

    print(t1-t0)

def run():
    t0=time.time()
    loop = asyncio.new_event_loop()
    tasks = [hello() for i in range(10)]

    loop.run_until_complete(asyncio.wait(tasks))

    loop.close()
    t1=time.time()

    print(t1-t0)
th=threading.Thread(target=run)
th.start()
th.join()
time.sleep(2)