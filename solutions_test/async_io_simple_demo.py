import threading
import asyncio
import time


async def hello():
    print('Hello world! (%s)' % threading.currentThread())
    await asyncio.sleep(1)# 运行到这一步之后，程序开始重新运行。
    print('Hello again! (%s)' % threading.currentThread())

t0=time.time()
loop = asyncio.get_event_loop()
tasks = [hello() for i in range(10)]

loop.run_until_complete(asyncio.wait(tasks))

loop.close()
t1=time.time()
print(t1-t0)