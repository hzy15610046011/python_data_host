# -*- coding: utf-8 -*-
# 首先，运行run_server.py，启动服务器。
# 当run_server.py运行时，运行此文件。
# 默认端口为4000
from pyjrdatahost.client import set_data,get_data
import numpy as np
set_data(name='a',data='this is a test message')
print('a is:',get_data('a'))
set_data('b',np.empty(shape=(1024*2,1024*2,3),dtype=np.uint16),debug=True)# 模拟，传输一张较大的图片。
print('b is:',get_data('b',debug=True))
