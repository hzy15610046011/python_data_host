from localdatahost.local_datahost import DataHost
import pandas as pd

dh = DataHost()
original_data_frame = pd.read_csv('class.csv').iloc[:2,[0,2]]# 取数据前2行

dh.set_data('test_file', original_data_frame)  # 添加数据。
dh.update_data_set_info(dataset_name='test_file', dataset=original_data_frame, path='class.csv', create_time='2017.10.10',
                        update_time='2019.10.10', row='19', col='5', remarks='',
                        file_size='811 bytes', memory_usage='8119 bytes', info='')  # 更新数据集信息

data_copy = dh.get_data('test_file', copy_val=True)  # 获取信息，并且复制。默认值是True.
data_ref = dh.get_data('test_file', copy_val=False)  # 获取信息，但是不复制(也就是获取引用)

print('copy_val=True时，获取的数据是否为原对象？',original_data_frame is data_copy)
print('copy_val=False时，获取的数据是否为原对象？',original_data_frame is data_ref)


#尽管数据对象不是同一个，但是值是一样的。
# 当修改data_ref的时候，original_data_frame也会跟着变，因为是同一个对象。此时data_copy不受影响。
print('\n将data_ref第2行年龄设置为20')
data_ref.at[1,'Age']=20
data_copy.at[0,'Age']=20
print('引用获取的数据:\n',data_ref)
print('复制获取的数据：\n',data_copy)
print('原始的数据(发生与data_ref相同，与data_copy不同的变化):\n',data_ref)

print('数据信息（仅为测试数据，与文件实际不同）:', dh.all_data_sets_info['test_file'])
print('数据创建时间：',dh.all_data_sets_info['test_file']['create_time'])
